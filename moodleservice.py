import os
import json
import requests
from flask import Flask, request, Response

mtoken = '2e880d588c7201ebd8796b837d3699f8'
Surl = 'http://localhost/moodle/webservice/rest/server.php'
token = 'eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjRkMmZiNmY3NDZmOTAwMWExZGMwMmQiLCJlbWFpbCI6ImFkYW0iLCJmaXJzdE5hbWUiOiJBZGFtIiwibGFzdE5hbWUiOiJMZW1tb24iLCJyb2xlcyI6WyJhZG1pbiJdLCJvcmdhbml6YXRpb25zIjp7ImRlZmF1bHQiOnsiX2lkIjoiNWU4NzUyNGQwOThiZTIyYWI1NTQyNjk5IiwibmFtZSI6IkNvbnZlcmdlbmNlLnRlY2giLCJkZXNjcmlwdGlvbiI6IlNvbWUgZGVzY3JpcHRpb24iLCJkb21haW5zIjp7ImRlZmF1bHQiOiJodHRwczovL2NvbnZlcmdlbmNlLnRlY2gifSwiaW1hZ2VVcmxzIjp7ImRlZmF1bHQiOiJodHRwczovL3Jlcy5jbG91ZGluYXJ5LmNvbS9kYWRscWZyeXQvaW1hZ2UvdXBsb2FkL3YxNTcxNDUxMDcyL2NvbnZlcmdlbmNlLWxvZ29zL0NvbnZlcmdlbmNlX0xvZ29fRmluYWwtMDFfdHE1d3RkLnBuZyJ9LCJhbGlhc2VzIjpbIkNvbnZlcmdlbmNlIl0sInJlbmRlcmVycyI6eyJkZWZhdWx0IjoiMTIzNDUifSwiY2VydGlmaWNhdGVTdG9yZXMiOnsiZGVmYXVsdCI6eyJyZWxheSI6IjB4YUU2MTE0OEI4QkQyMDQwNDdmMGY1RDNFMmQyMWE3YzIxOGZhOGRENCIsInN0b3JlIjoiMHg4MDQ0Mzc3NzA2RTAzNTg3MzVmMTg4MjE3M2E0OGYzRTNkN0ZiN0JkIn19LCJwdWJsaWNLZXlzIjp7ImRlZmF1bHQiOiIweDhFMTgzNTgxYTAzNDIwZDk5YURFQUEyOThBQ2U5YjIwM0MxMTk2ZDQifSwiYXNzZXRzIjpbeyJwcm92aWRlciI6ImF3cy1zMyIsInR5cGUiOiJpbWFnZS9qcGVnIiwia2V5IjoiaHR0cHM6Ly9zMy5jYS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbS90cnliZS5wdWJsaWMuYXNzZXRzL2Rldi9Db252ZXJnZW5jZS50ZWNoLzVnQm82Vzh0RHlNWmNMQm5CNHpQcXcuanBlZyJ9LHsicHJvdmlkZXIiOiJhd3MtczMiLCJ0eXBlIjoiaW1hZ2UvcG5nIiwia2V5IjoiaHR0cHM6Ly9zMy5jYS1jZW50cmFsLTEuYW1hem9uYXdzLmNvbS90cnliZS5wdWJsaWMuYXNzZXRzL2Rldi9Db252ZXJnZW5jZS50ZWNoLzVnQm82Vzh0RHlNWmNMQm5CNHpQcXcucG5nIn0seyJwcm92aWRlciI6ImF3cy1zMyIsInR5cGUiOiJpbWFnZS9wbmciLCJrZXkiOiJodHRwczovL3MzLmNhLWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL3RyeWJlLnB1YmxpYy5hc3NldHMvZGV2L0NvbnZlcmdlbmNlLnRlY2gvOWhnTXBDQzJIVWsyem9QY282OGp4Yy5wbmcifV19fSwic3ViIjoiaXNzdWVyIiwiYXVkIjoiaHR0cHM6Ly92YXVsdC50cnliZS5pZCIsImlzcyI6InZhdWx0LnRyeWJlLmlkIiwiYWNjb3VudF9pZCI6ImFkYW0iLCJpYXQiOjE1OTkwODUyMTYsImV4cCI6MTU5OTE3MTYxNn0.p7hdx-0fSzw6KfiKhT8hrcxvY0Jk7CZumivsf7PrRC9l6aKtGT-bQU6CLjrp_MVUZVNuyDW4eI2QBU3mgSFftk0mYm0zNswAUGYJwIvgEtjRtdFEWIKo8szcWco2XWTsbabU5kdz5wKjszeqjzJ_EI8dmNcLAUVA7_fFgxPu5p7AhbjX8juPPPV_HMWYVbTo8Tgx3-jOhzLgka4Lx5W8SF905cNxwB4Ac2JbC9NkWcD24MeQWc7DVO_29UXdVisACkNAMeGBdo_PSKmEGJbcQLvAqxSSizfTlihQhrZbpCGSB_8JSs_mC2vjWU9KSTjsj1_Z8C0iBctO_zjcB1VERQ'
Turl = 'https://api.dev.trybe.id'
endpoint = '/issuer/issueAndPublishCertificates'


app = Flask(__name__)
#Flask service for receiving POST from moodle through ngrok
@app.route('/', methods=['POST','GET'])
def respond():
    if request.method == 'POST':
        IssueCred(request.json)
        return '', 200
    else:
        abort(400)


def IssueCred(r):
    userid=r['userid']
    courseid=r['courseid']

    #request to moodle for user info
    tempjson = json.loads(requests.get(Surl,params= {
        "wstoken":mtoken,
        "moodlewsrestformat":"json",
        "wsfunction":"core_user_get_users",
        "criteria[0][key]":"id",
        "criteria[0][value]":userid
        }).text)

    #load fake credential payload
    a_file = open('rbody.json',"r")
    json_object=json.load(a_file)
    a_file.close()

    #add info from user to credential payload
    json_object['certs'][0]['recipient']['name'] = tempjson['users'][0]['fullname']
    json_object['certs'][0]['recipient']['email'] = tempjson['users'][0]['email']

    #generate and send credential to user's email
    r = requests.post(Turl+endpoint, headers={'authorization':"Bearer "+token},json = json_object)
    print("Credential sent to",email)


if __name__ == '__main__':
    #app.debug = True
    host = os.getenv('IP','127.0.0.1')
    port = int(os.getenv('PORT',3000))
    app.run(host=host,port=port)
